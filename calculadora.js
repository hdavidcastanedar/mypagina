let valor = '';
let operador = '';
let memoria = '';

function agregarValor(digito) {
  valor += digito;
  document.getElementById('resultado').value = valor;
}

function agregarOperador(op) {
  if (valor !== '') {
    if (memoria === '') {
      memoria = valor;
    } else {
      calcular();
    }
    operador = op;
    valor = '';
  }
}

function calcular() {
  if (operador === '+') {
    memoria = parseFloat(memoria) + parseFloat(valor);
  } else if (operador === '-') {
    memoria = parseFloat(memoria) - parseFloat(valor);
  } else if (operador === '*') {
    memoria = parseFloat(memoria) * parseFloat(valor);
  } else if (operador === '/') {
    memoria = parseFloat(memoria) / parseFloat(valor);
  } else {
    memoria = valor;
  }
  valor = '';
  operador = '';
  document.getElementById('resultado').value = memoria.toFixed(2);
}

function borrar() {
  valor = '';
  operador = '';
  memoria = '';
  document.getElementById('resultado').value = '';
}
